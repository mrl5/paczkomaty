from abc import ABCMeta, abstractmethod


class MachineVisitor(metaclass=ABCMeta):
    """ formal interface """

    @classmethod
    def __subclasshook__(cls, subclass):
        return (
            hasattr(subclass, "visitBox")
            and callable(subclass.visitBox)
            and hasattr(subclass, "visitBoxGroup")
            and callable(subclass.visitBoxGroup)
            and hasattr(subclass, "visitPackMachine")
            and callable(subclass.visitPackMachine)
        )

    @abstractmethod
    def visitBox(self, box) -> None:
        raise NotImplementedError

    @abstractmethod
    def visitBoxGroup(self, boxGroup) -> None:
        raise NotImplementedError

    @abstractmethod
    def visitPackMachine(self, packMachine) -> None:
        raise NotImplementedError
