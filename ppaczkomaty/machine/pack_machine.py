from machine.machine_free_space_visitor import MachineFreeSpaceVisitor
from operations.retrieve_pack_operation import RetrievePackOperation


class PackMachine:
    def __init__(self, id):
        self.id = id
        self.groups = []
        self.numberOfEmptyBoxes = None

    def put(self, pack):
        for group in self.groups:
            box = group.getSmallestEmptyBox(pack.size)
            if box is not None:
                box.put(pack)
                return

    def add(self, boxGroup):
        self.groups.append(boxGroup)

    def clear(self):
        self.groups = []

    def accept(self, visitor):
        self.numberOfEmptyBoxes = visitor.visitPackMachine(self)
        for group in self.groups:
            group.accept(visitor)

    def getNumberOfEmptyBoxes(self):
        visitor = MachineFreeSpaceVisitor()
        self.accept(visitor)
        return self.numberOfEmptyBoxes
