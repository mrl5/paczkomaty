from pack.pack_size import PackSize
from .machine_visitor import MachineVisitor


class MachineFreeSpaceVisitor(MachineVisitor):
    def __init__(self):
        self.quantities = {PackSize.A: 0, PackSize.B: 0, PackSize.C: 0}
        self.totalFreeBoxes = 0

    def visitBox(self, box):
        """ odwiedza przegrode """
        if box.isEmpty():
            self.quantities[box.size] += 1

    def visitBoxGroup(self, boxGroup):
        pass

    def visitPackMachine(self, packMachine):
        pass
