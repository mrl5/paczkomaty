from .price_strategy_factory import PriceStrategyFactory


class PriceService:
    def __init__(self):
        self.strategyFactory = PriceStrategyFactory()
        self.priceStrategy = None

    def price(self):
        return self.priceStrategy.price()

    def setPackSize(self, size):
        self.priceStrategy = self.strategyFactory.getPriceStrategy(size)
